#!/usr/bin/python

import web
import time
from threading import Timer
import usbnetpower8800

urls = ( '/','status',
         '/power[/]*', 'power')

try:
    pwr=None
    while not pwr:
        pwr=usbnetpower8800.Power()
        time.sleep(30)
except ValueError as e:
    print "Device not found.  Trying again in 30 seconds"
    pass


def PowerOff(action):
    print action
    print "Power Off"
    pwr.Set(False)


class status:
    def GET(self):
        return "Power Service OK"

class power:
    def GET(self):
        try:
            if pwr.IsOn() == True:
                return "Power is ON"
            else:
                return "Power is OFF"
        except ValueError as e:
            return "Device not found.  Check cabling and try again"


    def POST(self):
        data=web.input(cmd='',duration=0)
        try:
            if data.cmd=='ON':
                if data.duration > 0:
                   t = Timer(float(data.duration), pwr.Set,[False,])
                   #t = Timer(data.duration, PowerOff,False)
                   t.start()
                pwr.Set(True)
                return "POWER ON"
            elif data.cmd=='OFF':
                print "Turning power off"
                pwr.Set(False)
                return "POWER OFF"

        except ValueError as e:
            return "Device not found.  Check cabling and try again"

app = web.application(urls, globals())

if __name__ == "__main__":
    app.run()
